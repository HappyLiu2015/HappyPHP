/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50520
Source Host           : localhost:3306
Source Database       : happyliu

Target Server Type    : MYSQL
Target Server Version : 50520
File Encoding         : 65001

Date: 2015-12-08 11:02:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `happy_menu`
-- ----------------------------
DROP TABLE IF EXISTS `happy_menu`;
CREATE TABLE `happy_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键自增',
  `tag` varchar(20) NOT NULL COMMENT '菜单唯一标识',
  `icon` varchar(60) NOT NULL DEFAULT 'inventory' COMMENT '菜单图标样式',
  `pid` int(10) NOT NULL DEFAULT '0' COMMENT '父ID',
  `level` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1为一级 2为分组 3为三级',
  `text` varchar(50) NOT NULL COMMENT '菜单名称',
  `homepage` varchar(50) NOT NULL COMMENT '一级菜单默认的URL',
  `href` varchar(100) NOT NULL COMMENT '菜单URL地址',
  `closeable` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否能关闭，默认都能关闭，只有首页系统信息不能',
  `add_time` int(10) NOT NULL COMMENT '添加日期',
  `sort` int(10) NOT NULL DEFAULT '100' COMMENT '排序',
  PRIMARY KEY (`id`),
  UNIQUE KEY `tag` (`tag`),
  KEY `sort` (`sort`)
) ENGINE=InnoDB AUTO_INCREMENT=1010 DEFAULT CHARSET=utf8 COMMENT='菜单表(权限节点表)';

-- ----------------------------
-- Records of happy_menu
-- ----------------------------
INSERT INTO `happy_menu` VALUES ('1000', 'home', 'home', '0', '1', '首页', 'main', '', '1', '1448008527', '100');
INSERT INTO `happy_menu` VALUES ('1001', 'syspro', 'monitor', '1000', '2', '系统操作', '', '', '1', '1448008547', '100');
INSERT INTO `happy_menu` VALUES ('1002', 'userpro', 'monitor', '1000', '2', '用户操作', '', '', '1', '1448008557', '100');
INSERT INTO `happy_menu` VALUES ('1003', 'main', 'monitor', '1001', '3', '后台首页', '', '/index/main', '0', '1448008573', '100');
INSERT INTO `happy_menu` VALUES ('1004', 'menu', 'monitor', '1001', '3', '菜单管理', '', '/menu/index', '1', '1448008793', '100');
INSERT INTO `happy_menu` VALUES ('1005', 'usergroup', 'monitor', '1002', '3', '用户列表', '', '/user/index', '1', '1448008987', '100');
INSERT INTO `happy_menu` VALUES ('1007', 'test', 'supplier', '0', '1', '测试', 'testpro_menu', '', '1', '1448437029', '100');
INSERT INTO `happy_menu` VALUES ('1008', 'testpro', 'inventory', '1007', '2', '常规操作', '', '', '1', '1448437062', '100');
INSERT INTO `happy_menu` VALUES ('1009', 'testpro_menu', 'inventory', '1008', '3', '测试功能', '', '/Test/index', '1', '1448437118', '100');

-- ----------------------------
-- Table structure for `happy_user`
-- ----------------------------
DROP TABLE IF EXISTS `happy_user`;
CREATE TABLE `happy_user` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户UID',
  `username` varchar(60) NOT NULL COMMENT '用户名',
  `password` char(32) NOT NULL COMMENT '用户密码',
  `nickname` varchar(50) NOT NULL COMMENT '游戏中的昵称',
  `credit` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户积分',
  `level` int(10) NOT NULL DEFAULT '0' COMMENT '游戏中的等级',
  `logo` varchar(200) DEFAULT NULL COMMENT '用户头像',
  `addtime` int(10) DEFAULT NULL COMMENT '用户添加日期整型',
  `addtime_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '用户添加日期 日期格式',
  `updatetime` int(10) DEFAULT NULL COMMENT '用户信息最后一次更新时间',
  `sex` tinyint(1) DEFAULT '0' COMMENT '用户的性别，值为1时是男性，值为2时是女性，值为0时是未知',
  `city` varchar(50) DEFAULT NULL COMMENT '用户所在城市',
  `province` varchar(50) DEFAULT NULL COMMENT '用户所在省份',
  `country` varchar(50) DEFAULT NULL COMMENT '用户所在国家',
  `last_login_ip` int(11) NOT NULL COMMENT '最后一次登录IP',
  `last_login_time` int(10) NOT NULL COMMENT '最后一次登录时间',
  `login` int(11) NOT NULL COMMENT '登录次数',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '用户状态 1正常 2禁用',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of happy_user
-- ----------------------------
INSERT INTO `happy_user` VALUES ('1', 'admin', '90f390f243c6db119a08b0ca680deeb8', 'happy', '11', '15', 'http://wx.qlogo.cn/mmopen/JiavaWZxX4Yuf505hDv58TXOmHYj0ARPVmkjXnfPkbokUo4tD1MJ8jXce7utiakbibur2e5w3SyGR981K6lu4ylCjAs0RPdxQNI/0', '1447809580', '2015-11-18 09:19:40', '1449543644', '1', '宁波', '浙江', '中国', '2130706433', '1449543657', '14', '1');